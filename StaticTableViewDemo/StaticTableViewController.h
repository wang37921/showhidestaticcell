//
//  StaticTableViewController.h
//  StaticTableViewDemo
//
//  Created by Marty Dill on 12-02-28.
//  Copyright (c) 2012 Marty Dill. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StaticTableViewController : UITableViewController

- (IBAction)HideButtonTouch:(id)sender;

- (IBAction)ShowButtonTouch:(id)sender;

@property (nonatomic, assign) bool hideTableSection;

@end
