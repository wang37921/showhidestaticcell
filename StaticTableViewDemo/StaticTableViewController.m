//
//  StaticTableViewController.m
//  StaticTableViewDemo
//
//  Created by Marty Dill on 12-02-28.
//  Copyright (c) 2012 Marty Dill. All rights reserved.
//

#import "StaticTableViewController.h"


@implementation StaticTableViewController

@synthesize hideTableSection;


-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if(section == 1 && hideTableSection)
        return [[UIView alloc] initWithFrame:CGRectZero];    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // warning: The number must be match with deleteRowsAtIndexPaths; insertRowsAtIndexPaths function invoke.
    if(section == 1)
    {
        if(hideTableSection)
            return 1;
        else
            return 3;
    }
    else
    {
        return 2;
    }
}

- (IBAction)HideButtonTouch:(id)sender
{
    if (hideTableSection != YES) {
        hideTableSection = YES;
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:1 inSection:1], [NSIndexPath indexPathForRow:2 inSection:1], nil] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
    }
}

- (IBAction)ShowButtonTouch:(id)sender
{
    if (hideTableSection != NO) {
        hideTableSection = NO;
        [self.tableView beginUpdates];
        [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:1 inSection:1], [NSIndexPath indexPathForRow:2 inSection:1], nil] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
    }
}

@end
